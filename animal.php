<?php

    class Animal{
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";
        public function set_name($name) {
            $this->name = $name;
        }
        public function get_name() {
            return $this->name;
        }
    }

?>