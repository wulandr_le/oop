<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    echo "<h4>Soal Nomor 1</h4>";
    
    $sheep = new Animal();
    $sheep->set_name('shaun');
    echo "Name: " . $sheep->get_name() . "<br>";
    echo "legs: " . $sheep->legs . "<br>";
    echo "cold blooded: " . $sheep->cold_blooded . "<br>";

    echo "<br>";
    echo "<h4>Soal Nomor 2</h4>";

    $kodok = new Frog;
    $kodok->set_name('buduk');
    echo "Name: " . $kodok->get_name() . "<br>";
    echo "legs: " . $kodok->legs . "<br>";
    echo "cold blooded: " . $kodok->cold_blooded . "<br>";
    $kodok->jump() ;

    echo "<br>";
    echo"<br>";

    $sungokong = new Ape();
    $sungokong->set_name('kera sakti');
    echo "Name: " . $sungokong->get_name() . "<br>";
    echo "legs: " . $sungokong->legs . "<br>";
    echo "cold blooded: " . $sungokong->cold_blooded . "<br>";
    $sungokong->yell() ; 

?>